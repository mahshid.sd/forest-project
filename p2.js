var j = 0;  //number of Nodes
function addNode(x) {
    var newNode = document.createElement("div");
    j++;
    newNode.id = "Node_" + j;
    newNode.innerHTML = `
    <p>Node ${j}</p>
    <button class="button" onclick=addNode(this.parentElement.id)><i class="fas fa-plus"></i></button>
    <button class="button" onclick=deleteNode(this.parentElement.id)><i class="fas fa-minus"></i></button>
    `
    if(x==='body') {
        document.body.appendChild(newNode);
    }else {
        document.getElementById(x).appendChild(newNode);
        newNode.style.backgroundColor = `rgba(1,1,1,0.2)`
    }
}


function deleteNode(x){
    document.getElementById(x).remove();
}
